package lab11;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordCount = new HashMap<String, Integer>();
	
	public WordCounter(String m){
	this.message = m;
	}
	
	public void count() {
		String[] s = this.message.split(" ");
		HashSet<String> str = new HashSet<String>();
		for(String a : s){
			str.add(a);
		}
		for(String b : str){
			int count = 0;
			for (int i = 0 ; i< s.length ; i++){
				if(b.equals(s[i])){
					count++;
				}
			}
			wordCount.put(b, count);
		}

		
	}
	
	public int hasWord(String word) {
		return wordCount.get(word);
	}
}